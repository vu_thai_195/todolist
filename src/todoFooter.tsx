import { ACTIVE_TODOS, ALL_TODOS, COMPLETED_TODOS } from "./constants";
import classNames from "classnames";

const TodoFooter: React.FC<ITodoFooterProps> = ({ count, nowShowing, completedCount, onClearCompleted, onShowAll, onShowActive, onShowCompleted }) => {

  const activeTodoWord = (count: number, word: string) => {
    return count === 1 ? word : word + 's';
  }

  const clearButton = completedCount > 0 ? (
    <button
      className="clear-completed"
      onClick={onClearCompleted}>
      Clear completed
    </button>
  ) : <></>;

  return (
    <footer className="footer">
      <span className="todo-count">
        <strong>{count}</strong> {activeTodoWord(count, 'item')} left
        </span>
      <ul className="filters">
        <li>
          <a
            href="#/"
            onClick={onShowAll}
            className={classNames({ selected: nowShowing === ALL_TODOS })}>
            All
            </a>
        </li>
        {' '}
        <li>
          <a
            href="#/active"
            onClick={onShowActive}
            className={classNames({ selected: nowShowing === ACTIVE_TODOS })}>
            Active
            </a>
        </li>
        {' '}
        <li>
          <a
            href="#/completed"
            onClick={onShowCompleted}
            className={classNames({ selected: nowShowing === COMPLETED_TODOS })}>
            Completed
            </a>
        </li>
      </ul>
      {clearButton}
    </footer>
  )
}

export default TodoFooter;