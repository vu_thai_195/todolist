
/// <reference path="./interfaces.d.ts"/>

import classNames from "classnames";
import React, { useEffect, useRef, useState } from "react";
import { ENTER_KEY, ESCAPE_KEY } from "./constants";

const TodoItem: React.FC<ITodoItemProps> = ({ todo, onEdit, onSave, onToggle, onDestroy, onCancel, editing }) => {
  const [editText, setEditText] = useState(todo.title);
  const editField = useRef<any>(null);
  const prevEditingRef = useRef(editing);
  useEffect(() => {
    prevEditingRef.current = editing;
    if (!prevCount && editing) {
      editField.current.focus();
    }
  });
  const prevCount = prevEditingRef.current;

  const handleSubmit = () => {
    const val = editText.trim();
    if (val) {
      onSave(val);
      setEditText(val);
    } else {
      onDestroy();
    }
  }
  const handleEdit = () => {
    onEdit();
    setEditText(todo.title);
  }

  const handleKeyDown = (event: React.KeyboardEvent) => {
    if (event.keyCode === ESCAPE_KEY) {
      setEditText(todo.title);
      onCancel(event);
    } else if (event.keyCode === ENTER_KEY) {
      handleSubmit();
    }
  }

  const handleChange = (event: React.FormEvent) => {
    var input: any = event.target;
    setEditText(input.value);
  }

  return (
    <li className={classNames({
      completed: todo.completed,
      editing: editing
    })}>
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={todo.completed}
          onChange={onToggle}
        />
        <label onDoubleClick={e => handleEdit()}>
          {todo.title}
        </label>
        <button className="destroy" onClick={onDestroy} />
      </div>
      <input
        ref={editField}
        className="edit"
        value={editText}
        onBlur={e => handleSubmit()}
        onChange={e => handleChange(e)}
        onKeyDown={e => handleKeyDown(e)}
      />
    </li>)
}
export default TodoItem;