import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import AppTodo from './App';
import { TodoModel } from "./todoModel"
import reportWebVitals from './reportWebVitals';
var model = new TodoModel();
function render() {
  ReactDOM.render(
    <React.StrictMode>
      <AppTodo model={model}/>
    </React.StrictMode>,
    document.getElementById('root')
  );
}
model.subscribe(render);
render();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
