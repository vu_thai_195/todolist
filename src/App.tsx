/// <reference path="./interfaces.d.ts"/>

import { useRef, useState, createElement } from "react";
import { ACTIVE_TODOS, ALL_TODOS, COMPLETED_TODOS, ENTER_KEY } from "./constants";
import TodoFooter from "./todoFooter";
import TodoItem from "./todoItem";

const AppTodo: React.FC<IAppProps> = ({ model }) => {
  const [nowShowing, setNowShowing] = useState(ALL_TODOS);
  const [editing, setEditing] = useState<string|null>(null);
  const todos = model.todos;
  const newField = useRef<any>(createElement('input'));

  const toggleAll = (event: any) => {
    var target: any = event.target;
    var checked = target.checked;
    model.toggleAll(checked);
  }

  const toggle = (todoToToggle: ITodo) => {
    model.toggle(todoToToggle);
  }

  const destroy = (todo: ITodo) => {
    model.destroy(todo);
  }

  const edit = (todo: ITodo) => {
    setEditing(todo.id || null);
  }

  const save = (todoToSave: ITodo, text: String) => {
    model.save(todoToSave, text);
    setEditing(null);
  }

  const cancel = () => {
    setEditing(null);
  }

  const clearCompleted = () => {
    model.clearCompleted();
  }

  const handleNewTodoKeyDown = (event: React.KeyboardEvent) => {
    if (event.keyCode !== ENTER_KEY) {
      return;
    }

    event.preventDefault();
    if (!newField || !newField.current) {
      return;
    }
    const val = newField.current.value.trim();

    if (val) {
      model.addTodo(val);
      newField.current.value = '';
    }
  }

  var shownTodos = todos.filter((todo: ITodo) => {
    switch (nowShowing) {
      case ACTIVE_TODOS:
        return !todo.completed;
      case COMPLETED_TODOS:
        return todo.completed;
      default:
        return true;
    }
  });

  const todoItems = shownTodos.map((todo: ITodo) => {
    return (
      <TodoItem
        key={todo.id}
        todo={todo}
        onToggle={() => toggle(todo)}
        onDestroy={() => destroy(todo)}
        onEdit={() => edit(todo)}
        editing={editing === todo.id}
        onSave={(val) => save(todo, val)}
        onCancel={cancel}
      />
    );
  });

  const activeTodoCount = todos.reduce((accum: number, todo: ITodo) => {
    return todo.completed ? accum : accum + 1;
  }, 0);

  const main = (
    <section className="main">
      <input
        id="toggle-all"
        className="toggle-all"
        type="checkbox"
        onChange={e => toggleAll(e)}
        checked={activeTodoCount === 0}
      />
      <label htmlFor="toggle-all">Mark all as complete</label>
      <ul className="todo-list">
        {todoItems}
      </ul>
    </section>
  );

  const completedCount = todos.length - activeTodoCount;

  const footer = (activeTodoCount || completedCount) ? (
    <TodoFooter
      count={activeTodoCount}
      completedCount={completedCount}
      nowShowing={nowShowing}
      onClearCompleted={() => clearCompleted()}
      onShowActive={() => setNowShowing(ACTIVE_TODOS)}
      onShowAll={() => setNowShowing(ALL_TODOS)}
      onShowCompleted={() => setNowShowing(COMPLETED_TODOS)}
    />) : <></>;

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          ref={newField}
          className="new-todo"
          placeholder="What needs to be done?"
          onKeyDown={e => handleNewTodoKeyDown(e)}
          autoFocus={true}
        />
      </header>
      {main}
      {footer}
    </section>
  )
}

export default AppTodo;