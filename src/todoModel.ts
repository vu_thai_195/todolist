import { CACHE_KEY } from "./constants";

class TodoModel {
  public todos: ITodo[];
  public onChanges: any[];

  constructor() {
    this.todos = this.getTodos();
    this.onChanges = [];
  }

  public subscribe(onChange: () => void) {
    this.onChanges.push(onChange);
  }

  public uuid = () => {
    var i, random;
    var uuid = '';
    for (i = 0; i < 32; i++) {
      random = Math.random() * 16 | 0;
      if (i === 8 || i === 12 || i === 16 || i === 20) {
        uuid += '-';
      }
      // eslint-disable-next-line
      uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
        .toString(16);
    }
    return uuid;
  }


  public addTodo = (title: string) => {
    this.todos = this.todos.concat({
      id: this.uuid(),
      title: title,
      completed: false
    });
    this.inform();
  }

  public inform = () => {
    if (this.todos) {
      localStorage.setItem(CACHE_KEY, JSON.stringify(this.todos));
      this.onChanges.forEach(cb => cb());
    }
  }

  public toggleAll = (checked: boolean) => {
    this.todos = this.todos.map<ITodo>((todo: ITodo) => {
      return { ...todo, completed: checked };
    });

    this.inform();
  }

  public toggle = (todoToToggle: ITodo) => {
    this.todos = this.todos.map<ITodo>((todo: ITodo) => {
      return todo !== todoToToggle ?
        todo : { ...todo, completed: !todo.completed };
    });

    this.inform();
  }

  public destroy = (todo: ITodo) => {
    this.todos = this.todos.filter(candidate => {
      return candidate !== todo;
    });

    this.inform();
  }

  public save = (todoToSave: ITodo, text: string) => {
    this.todos = this.todos.map(function (todo) {
      return todo !== todoToSave ? todo : { ...todo, title: text };
    });

    this.inform();
  }

  public clearCompleted = () => {
    this.todos = this.todos.filter(function (todo) {
      return !todo.completed;
    });
    this.inform();
  }

  public getTodos = () => {
    const store = localStorage.getItem(CACHE_KEY);
    return ((store && JSON.parse(store)) || []) as ITodo[];
  }
}

export { TodoModel };